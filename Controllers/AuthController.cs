﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Configuration;
using System.Security.Cryptography;
using System.Security.Claims;
using System.Text;
using T3_Diars.BDD;
using T3_Diars.Models;
using Microsoft.AspNetCore.Authorization;

namespace T3_Diars.Controllers
{
    public class AuthController : Controller
    {
        private EjercicioContext context;

        private IConfiguration configuration;

        public AuthController(EjercicioContext context, IConfiguration config)
        {
            this.context = context;
            this.configuration = config;
        }
        /**/
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            var user = context.usuarios
                .FirstOrDefault(o => o.nom_Usuario == username && o.pass_Usuario == CreateHash(password));
            if (user == null)
            {
                TempData["AuthMessage"] = "Usuario o contraseña incorrecto";
                return RedirectToAction("Login");
            }
            //Autenticación del usuario
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.nom_Usuario),
            };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            HttpContext.SignInAsync(claimsPrincipal);
            
            return RedirectToAction("MostrarRutinas", "Ejercicio");
        }
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Login");
        }

        public string Create(string password)
        {
            return CreateHash(password);
        }

        private string CreateHash(string input)
        {
            input += configuration.GetValue<string>("Key");
            var sha = SHA512.Create();

            var bytes = Encoding.Default.GetBytes(input);
            var hash = sha.ComputeHash(bytes);

            return Convert.ToBase64String(hash);
        }
        /**/
        [HttpGet]
        public IActionResult CrearCuenta()
        {
            return View();
        }
        [HttpPost]
        public IActionResult CrearCuenta(string username, string password)
        {
            string pass = CreateHash(password);
            // no hay mensajes => 0 mensaje
            Usuario user = new Usuario
            {
                nom_Usuario = username,
                pass_Usuario = pass
            };

            context.usuarios.Add(user);
            context.SaveChanges();
            return View();
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
