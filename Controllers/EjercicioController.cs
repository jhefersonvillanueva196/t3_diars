﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3_Diars.BDD;
using Microsoft.AspNetCore.Authorization;
using T3_Diars.Models;
using Microsoft.AspNetCore.Http;
using T3_Diars.Models.Dificultad;

namespace T3_Diars.Controllers
{
    public class EjercicioController : Controller
    {
        
        private EjercicioContext context;

        private IDificultad oDificultad;

        private static Rutina rutTemporal { get; set; }

        public EjercicioController(EjercicioContext context)
        {
            this.context = context;
        }
        public class InfoRutina
        {
            public Rutina rut { get; set; }
            public List<EjerRutin> ejerruts { get; set; }

        }
        public List<Rutina> rutina = new List<Rutina>();
        [HttpGet]
        public IActionResult MostrarEjercicios()
        {
            var ejercicios = context.ejercicios;
            bool valor = true;
            if (ejercicios == null)
                valor = false;
            else
                valor = true;
            Console.WriteLine(valor);
            return View("MostrarEjercicios", ejercicios);
        }
        public IActionResult MostrarRutinas()
        {
            var rus = context.rutinas;
            rutina = rus.ToList();
            //var id = GetLoggedUser().id_Usuario;
            foreach (Rutina ru in context.rutinas)
            {
                Rutina rutinas = rutina.FirstOrDefault(r => r.id_Usuario == 1);
                rutina.Add(rutinas);
            }
            return View("MostrarRutinas", rutina);
        }
        /*private Usuario GetLoggedUser()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = context.usuarios.First(o => o.nom_Usuario == username);
            return user;
        }*/
        public List<EjerRutin> temp = new List<EjerRutin>();
        public List<Ejercicio> Ejercicios = new List<Ejercicio>();
        
        [HttpGet]
        public IActionResult NombreRutina()
        {
            return View();
        }
        [HttpPost]
        public IActionResult NombreRutinas(string rut, string dif)
        {
            Rutina ru = new Rutina
            {
                id_Usuario = 1,
                nom_Rutina = rut,
                dificultad = dif
            };
            context.rutinas.Add(ru);
            context.SaveChanges();
            return RedirectToAction("CrearRutina");
        }
        [HttpGet]
        public IActionResult CrearRutina()
        {
            var infoEjer = context.ejercicios;
            var infoRutina = context.rutinas;
            rutina = infoRutina.ToList();
            var rut = rutina.Last();
            string dif = rut.dificultad;
            Random rnd = new Random();
            int nro = 0;
            int id = 0;
            int tiempo = 0;
            switch (dif)
            {
                case "Principiante":
                    this.oDificultad = new Pricipiante();
                    for (int i = 0; i < oDificultad.Reglas(); i++)
                    {
                        id = rnd.Next(21);
                        tiempo = rnd.Next(60, 121);
                        var ejercicio = context.ejercicios;
                        Ejercicio ejer = ejercicio.FirstOrDefault(e => e.id_Ejercicio == id);
                        EjerRutin ejerRutin = new EjerRutin
                        {
                            id_Rutina=rut.id_Rutina,
                            nom_EjerRutin = ejer.nom_Ejercicio,
                            vid_EjerRutin = ejer.vid_Ejercicio,
                            duracion = tiempo
                        };
                        temp.Add(ejerRutin);
                        context.EjercicioRutinas.Add(ejerRutin);
                        context.SaveChanges();
                    }
                    break;
                case "Intermedio":
                    this.oDificultad = new Intermedio();
                    nro = oDificultad.Reglas();
                    for (int i = 0; i < oDificultad.Reglas(); i++)
                    {
                        id = rnd.Next(21);
                        tiempo = rnd.Next(60, 121);
                        var ejercicio = context.ejercicios;
                        Ejercicio ejer = ejercicio.FirstOrDefault(e => e.id_Ejercicio == id);
                        EjerRutin ejerRutin = new EjerRutin
                        {
                            id_Rutina = rut.id_Rutina,
                            nom_EjerRutin = ejer.nom_Ejercicio,
                            vid_EjerRutin = ejer.vid_Ejercicio,
                            duracion = tiempo
                        };
                        temp.Add(ejerRutin);
                        context.EjercicioRutinas.Add(ejerRutin);
                        context.SaveChanges();
                    }
                    break;
                case "Avanzado":
                    this.oDificultad = new Avanzado();
                    for (int i = 0; i < oDificultad.Reglas(); i++)
                    {
                        id = rnd.Next(21);
                        var ejercicio = context.ejercicios;
                        Ejercicio ejer = ejercicio.FirstOrDefault(e => e.id_Ejercicio == id);
                        EjerRutin ejerRutin = new EjerRutin
                        {
                            id_Rutina = rut.id_Rutina,
                            nom_EjerRutin = ejer.nom_Ejercicio,
                            vid_EjerRutin = ejer.vid_Ejercicio,
                            duracion = 120
                        };
                        temp.Add(ejerRutin);
                        context.EjercicioRutinas.Add(ejerRutin);
                        context.SaveChanges();
                    }
                    break;
            }
            return View("CrearRutina",temp);
        }
        /*public IActionResult InfoRutina(int idRutina)
        {
            
            return RedirectToAction("InfoRutina");
        }*/
    }    
}
