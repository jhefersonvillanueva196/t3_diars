﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3_Diars.Models;

namespace T3_Diars.BDD.Mapping
{
    public class EjerRutinMap: IEntityTypeConfiguration<EjerRutin>
    {
        public void Configure(EntityTypeBuilder<EjerRutin> builder)
        {
            builder.ToTable("EjerRutin");
            builder.HasKey(er => er.id_EjerRutin);
        }
    }
}
