﻿using Microsoft.EntityFrameworkCore;
using T3_Diars.BDD.Mapping;
using T3_Diars.Models;

namespace T3_Diars.BDD
{
    public class EjercicioContext : DbContext
    {
        public DbSet<Ejercicio> ejercicios { get; set; }
        public DbSet<Rutina> rutinas { get; set; }
        public DbSet<Usuario> usuarios { get; set; }
        public DbSet<EjerRutin> EjercicioRutinas { get; set; }

        public EjercicioContext(DbContextOptions<EjercicioContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new EjercicioMap());
            modelBuilder.ApplyConfiguration(new RutinaMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new EjerRutinMap());
        }
    }
}
