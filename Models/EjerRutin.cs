﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T3_Diars.Models
{
    public class EjerRutin
    {
        public int id_EjerRutin { get; set; }
        public int id_Rutina { get; set; }
        public string nom_EjerRutin { get; set; }
        public string vid_EjerRutin { get; set; }
        public int duracion { get; set; }
    }
}
