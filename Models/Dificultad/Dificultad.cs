﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T3_Diars.Models.Dificultad
{
    public interface IDificultad
    {
        public int Reglas();
    }
}
