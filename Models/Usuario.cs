﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T3_Diars.Models
{
    public class Usuario
    {
        public int id_Usuario { get; set; }
        public string nom_Usuario { get; set; }
        public string pass_Usuario{ get; set; }
    }
}
