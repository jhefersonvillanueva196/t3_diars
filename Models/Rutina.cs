﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T3_Diars.Models
{
    public class Rutina
    {
        public int id_Rutina { get; set; }
        public int id_Usuario { get; set; }
        public string nom_Rutina { get; set; }
        public string dificultad { get; set; }
    }
}
