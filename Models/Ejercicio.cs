﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T3_Diars.Models
{
    public class Ejercicio
    {
        public int id_Ejercicio { get; set; }
        public string nom_Ejercicio { get; set; }
        public string vid_Ejercicio { get; set; }
    }
}
